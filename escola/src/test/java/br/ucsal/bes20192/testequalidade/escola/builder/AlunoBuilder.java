package br.ucsal.bes20192.testequalidade.escola.builder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
//https://blog.codeleak.pl/2014/06/test-data-builders-and-object-mother.html
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;

public class AlunoBuilder {
	public static final Integer DEFAULT_MATRICULA = 20000;
	public static final Integer DEFAULT_ANONASCIMENTO = 1998;
	public static final String DEFAULT_NOME = "Renato Oliveira";
	public static final SituacaoAluno DEFAULT_SITUACAO = SituacaoAluno.ATIVO;
	
	private Integer matricula = DEFAULT_MATRICULA;
	private Integer anoNascimento = DEFAULT_ANONASCIMENTO;
	private String nome = DEFAULT_NOME;
	private SituacaoAluno situacao = DEFAULT_SITUACAO;
	
	private AlunoBuilder() {}
	
	public static AlunoBuilder alunoBuilder(){
		return new AlunoBuilder();
	}
	
	public AlunoBuilder comNome(String nome) {
		this.nome = nome;
		return this;
	}
	
	public AlunoBuilder comMatricula(Integer matricula) {
		this.matricula = matricula;
		return this;
	}
	
	public AlunoBuilder comAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
		return this;
	}
	
	public AlunoBuilder comSituacaAluno(SituacaoAluno situacao) {
		this.situacao = situacao;
		return this;
	}
	
	public AlunoBuilder ativo() {
		this.situacao = SituacaoAluno.ATIVO;
		return this;
	}

	public AlunoBuilder cancelado() {
		this.situacao = SituacaoAluno.CANCELADO;
		return this;
	}

	
	public static AlunoBuilder umAlunoAtivo() {
		return new AlunoBuilder().ativo();
	}
	
	public static AlunoBuilder umAluno() {
		return new AlunoBuilder();
	}
	
	public static AlunoBuilder umAlunoMaiorDeIdade() {
		return new AlunoBuilder().comAnoNascimento(1998);
	}

	
	public AlunoBuilder but() {
		return AlunoBuilder.alunoBuilder().comNome("Renato").comAnoNascimento(1998).comMatricula(200006677).comSituacaAluno(SituacaoAluno.ATIVO);
	}
	public Aluno build() {
		Aluno aluno = new Aluno();
		aluno.setMatricula(matricula);
		aluno.setNome(nome);
		aluno.setSituacao(situacao);
		aluno.setAnoNascimento(anoNascimento);
		return aluno;
	}

}
