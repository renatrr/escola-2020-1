package br.ucsal.bes20192.testequalidade.escola.business;

import org.junit.Assert;
import org.junit.Test;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class AlunoBOIntegradoTest {
	private static DateHelper dh = new DateHelper();
	private static AlunoDAO alunoDAO = new AlunoDAO();
	private static AlunoBO alunoBO = new AlunoBO(alunoDAO, dh);
	
	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 tera 16
	 * anos. Caso de teste 
	 * # | entrada 				 | saida esperada 
	 * 1 | aluno nascido em 2003 |	 * 16
	 */
	public static void main(String[] args) {
		alunoDAO.excluirTodos();
	}
	@Test
	public void testarCalculoIdadeAluno1() {		
		Aluno aluno = AlunoBuilder.umAluno().comAnoNascimento(2004).build();
		alunoDAO.salvar(aluno);
		Integer saidaEsperada = 16;//sa�da esperada
		AlunoBO alunoBO = new AlunoBO(alunoDAO, dh);
		Integer saidaAtual = alunoBO.calcularIdade(aluno.getMatricula());
		Assert.assertEquals(saidaEsperada, saidaAtual);
		

	}

	/**
	 * Verificar se alunos ativos sao atualizados.
	 */
	@Test
	public void testarAtualizacaoAlunosAtivos() {
		Aluno alunoEsperado = AlunoBuilder.umAlunoAtivo().build();
		alunoBO.atualizar(alunoEsperado);
		Aluno alunoAtual = alunoDAO.encontrarPorMatricula(alunoEsperado.getMatricula());
		Assert.assertEquals(alunoEsperado, alunoAtual);
	}

}
