package br.ucsal.bes20192.testequalidade.escola.builder;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;

public class AlunoObjectMother {

	public static Aluno umAlunoMaiorDeIdade() {
		Aluno a = new Aluno();
		a.setAnoNascimento(1998);
		a.setMatricula(20000);
		a.setNome("Renato");
		a.setSituacao(SituacaoAluno.ATIVO);
		return a;
	}
	
	public static Aluno umAlunoAtivo() {
		Aluno a = new Aluno();
		a.setAnoNascimento(1998);
		a.setMatricula(20000);
		a.setNome("Renato");
		a.setSituacao(SituacaoAluno.CANCELADO);
		return a;
	}

	public static Aluno umAlunoCancelado() {
		Aluno a = new Aluno();
		a.setAnoNascimento(1998);
		a.setMatricula(20000);
		a.setNome("Renato");
		a.setSituacao(SituacaoAluno.CANCELADO);
		return a;
	}
}
